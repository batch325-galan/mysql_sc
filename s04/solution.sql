--a. Artists that has letter in its name

SELECT * FROM artists WHERE name LIKE '%d%';

--b. Songs less than 3:50 minutes

SELECT * FROM songs WHERE length < 350;

--c.  Join the albums and songs table (show only album name, song name, and legth)

SELECT albums.album_title, songs.song_name, songs.length FROM albums JOIN songs ON albums.id = songs.album_id;

--d. Join the artists and albums table(Find all albums that has letter a in its name)

SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id WHERE albums.album_title LIKE '%a%';

--e. Sort the albums in Z-A order.(Show only the first 4 records)

SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

--f. Join the albums and songs tables (Sort albums from A-Z)

SELECT albums.album_title, albums.artist_id, COUNT(*) AS num_songs FROM albums INNER JOIN songs ON albums.id = songs.album_id GROUP BY albums.album_title ORDER BY albums.album_title;

