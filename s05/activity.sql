--1. customerName of the customers who are from the Philippines
	--Query:
	SELECT * FROM customers WHERE country="Philippines";  

--2.  Query
	SELECT customers.contactLastName, customers.contactFirstName FROM customers WHERE customerName LIKE "%La Rochelle Gifts%"; 

	SELECT customers.contactLastName, customers.contactFirstName FROM customers WHERE customerName"La Rochelle Gifts"; 

--3. Query
	SELECT products.productName, products.MSRP FROM products WHERE productName="The Titanic";     

--4 Query
	SELECT employees.firstName, employees.lastName FROM employees WHERE email="jfirrelli@classicmodelcars.com";  

--5 Query
	SELECT customers.customerName FROM customers WHERE state is NULL; 
--6 Query
	SELECT employees.firstName, employees.lastName, employees.email FROM employees WHERE lastName="Patterson" && firstName="Steve";

--7 Query
	SELECT customers.customerName, customers.country, customers.creditLimit FROM customers WHERE country!="USA" && creditLimit > 3000;
--8 Query
	SELECT customerNumber FROM orders WHERE comments LIKE '%DHL%';

--9 Query 
	SELECT productLines.productLine FROM productLines WHERE textDescription LIKE '%state of the art%';

--10 Query
	SELECT DISTINCT country FROM customers;

--11 Query
	SELECT DISTINCT status FROM orders;

--12 Query
	SELECT customers.customerName, customers.country FROM customers WHERE country = "USA" OR country="Canada" OR country="France";

--13 Query
	SELECT `employees`.`firstName`, `employees`.`lastName`, `offices`.`city` FROM `employees` LEFT JOIN `offices` ON `employees`.`officeCode` = `offices`.`officeCode` WHERE offices.city="Tokyo";
--14 Query
	SELECT `customers`.`customerName` FROM `customers` LEFT JOIN `employees` ON `customers`.`salesRepEmployeeNumber` = `employees`.`employeeNumber` WHERE employees.employeeNumber=1166;
--15 Query
	SELECT `products`.`productName`, `customers`.`customerName` FROM `products` , `customers` LEFT JOIN `orders` ON `orders`.`customerNumber` = `customers`.`customerNumber` WHERE customers.customerName="Baane Mini Imports";	

--16 Query
	SELECT `employees`.`firstName`, `employees`.`lastName`, `customers`.`customerName`, `offices`.`country`
	FROM `employees` LEFT JOIN `customers` ON `customers`.`salesRepEmployeeNumber` = `employees`.`employeeNumber` 
	LEFT JOIN `offices` ON `employees`.`officeCode` = `offices`.`officeCode` WHERE customers.country=offices.country;

--17 Query
	SELECT `products`.`productName`, `products`.`quantityInStock`, `productlines`.`productLine` FROM `products` INNER JOIN `productlines` ON `products`.`productLine` = `productlines`.`productLine` WHERE productLines.productLine="planes" && products.quantityInStock<1000;

--18 Query
	SELECT `customers`.`customerName` FROM `customers` WHERE customers.phone LIKE '%+81';