-- Add 5 artists:
INSERT INTO artists (name) VALUES ("Taylor Swift");
INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justin Bieber");
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO artists (name) VALUES ("Bruno Mars");

-- Add album to TS:
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Fearless", "2008-1-1", 1);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Fearless", 246, "Pop rock", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Love Story", 213, "Country pop", 5);


INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Red", "2012-1-1", 1);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("State of Grace", 253, "Rock, alternative rock, arena rock", 6);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Red", 204, "Country", 6);

-- Lady Gaga:
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("A Star Is Born", "2018-1-1", 2);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Black Eyes", 151, "Rock and roll", 7);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Shallow", 201, "Country, rock, folk rock", 7);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Born This Way", "2011-1-1", 2);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Born This Way", 252, "Electropop", 8);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Purpose", "2015-1-1", 2);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Sorry", 142, "Dancehall-poptropical housemoombahton", 9);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Believe", "2012-1-1", 3);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Boyfriend", 251, "Pop", 10);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Dangerous Woman", "2016-1-1", 4);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Into You", 242, "EDM house", 11);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Thank U, Next", "2019-1-1", 4);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Thank U Next", 156, "Pop, R&B", 12);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("24K Magic", "2016-1-1", 5);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("24K Magic", 207, "Funk, disco, R&B", 13);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Earth to Mars", "2011-1-1", 5);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Lost", 152, "Pop", 14);

-- [SECTION] ADVANCED Selects:

-- to exclude record
SELECT * FROM songs WHERE id != 11;

SELECT song_name, genre FROM songs WHERE id != 11;

-- Greater than or equal operator:
SELECT * FROM songs WHERE id >= 11;

-- Less than or equal operator
SELECT * FROM songs WHERE id <= 11;

-- What if I want those records with id that has  5 to 11:
SELECT * FROM songs WHERE id >= 5 AND id <=11;

SELECT * FROM songs WHERE id BETWEEN 5 AND 11;

-- GET specific IDs:
SELECT * FROM songs WHERE id = 11;

-- OR Operator:
SELECT * FROM songs WHERE id =1 OR id = 11 OR id = 5;


-- IN Operator:
SELECT * FROM songs WHERE id IN (1, 11, 5);

-- Find Partial matches:
-- Ends wit letter a
SELECT * FROM songs WHERE song_name LIKE "%a";
SELECT * FROM songs WHERE song_name LIKE "%e";

SELECT * FROM songs WHERE song_name LIKE "a%";


SELECT * FROM songs WHERE album_id LIKE "10_";

-- SORT record
-- Ascending
SELECT * FROm songs ORDER BY song_name ASC;

-- Descending

SELECT * FROM songs ORDER BY song_name DESC;


-- DISTINCT Records:
SELECT DISTINCT genre FROM songs;


-- [SECTION] Table joins:
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id;

-- JOIN albums table and songs table:
SELECT * FROM albums JOIN songs ON albums.id = songs.album_id;

SELECT albums.album_title, songs.song_name, songs.length FROM albums JOIN songs ON albums.id = songs.album_id;

SELECT * FROM artists 
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;

SELECT artists.name, albums.album_title, songs.song_name FROM artists 
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;



